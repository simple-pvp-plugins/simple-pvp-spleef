package com.simplepvpspleef;

import com.simplepvp.Game;
import com.simplepvp.util.SimpleRegion;
import com.simplepvp.util.SimpleUtil;
import org.bukkit.*;
import org.bukkit.block.Block;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.ProjectileHitEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.scheduler.BukkitTask;
import org.bukkit.scoreboard.Team;
import org.bukkit.util.Vector;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class SpleefGame extends Game {

    // The variables needed for the game.
    private boolean gracePeriod = false;
    private int graceTime = 0;
    private BukkitTask gracePeriodTask = null;
    private final ItemStack spleefShovel;

    // Initialise the game.
    public SpleefGame(String gameName, Material gameIcon, SpleefMapConfig mapConfig) {
        super(gameName, gameIcon, mapConfig); // Call the super.
        resetGame(); // Fix snow layer if crash occurred.
        spleefShovel = createSpleefShovel(); // Create the spleef shovel object.
    }

/*----------------------------------------------------------------------------------------------------------------------
                                                    Setup Methods
----------------------------------------------------------------------------------------------------------------------*/

    // Create the spleef shovel object.
    private ItemStack createSpleefShovel() {
        ItemStack item = new ItemStack(Material.DIAMOND_SHOVEL, 1);
        ItemMeta itemMeta = item.getItemMeta();
        assert itemMeta  != null;
        itemMeta .addEnchant(Enchantment.DURABILITY, 32000, true);
        itemMeta .addEnchant(Enchantment.DIG_SPEED, 32000, true);
        item.setItemMeta(itemMeta);
        return item;
    }

/*----------------------------------------------------------------------------------------------------------------------
                                                  Base Methods
----------------------------------------------------------------------------------------------------------------------*/

    @Override
    public boolean sendPlayer(Player player) {
        return super.sendPlayer(player);
    }

    @Override
    public boolean removePlayer(Player player) {
        // Only run if the player was removed from the game.
        if (super.removePlayer(player)) {
            checkGameWin();
            return true;
        }
        return false;
    }

    @Override
    protected void killPlayer(Player player, Player damager, EntityDamageEvent.DamageCause cause) {
        super.killPlayer(player, damager, cause);

        player.sendTitle(ChatColor.RED + "You Died!", "", 0, 20, 10);
    }

    @Override
    protected void respawnPlayer(Player player) {
        // Never respawn player
    }

    @Override
    protected void startGame() {
        // Teleport all the players and give them their items.
        for (Player player : getPlayers()) {
            player.teleport(((SpleefMapConfig) getMapConfig()).getPvpSpawnLocation());
            player.setGameMode(GameMode.SURVIVAL);
            player.getInventory().addItem(spleefShovel);
        }

        // Start the grace period so players can get ready.
        startGracePeriod();
    }

    // Check if the anybody has won the game.
    @Override
    protected void checkGameWin() {
        super.checkGameWin();
        if (gameStarted && !gameEnded) {
            List<Player> alivePlayers = new ArrayList<>();

            // If a player is no longer in survival then they are not alive.
            for (Player player : getPlayers()) {
                if (player.getGameMode().equals(GameMode.SURVIVAL)) {
                    alivePlayers.add(player);
                }
                if (alivePlayers.size() > 0) {
                    break;
                }
            }

            if (alivePlayers.size() == 1) {
                endGame(alivePlayers.get(0));
            } else if (getPlayers().size() == 0) {
                endGame(null);
            }
        }
    }

    // End the game.
    protected void endGame(Player player) {
        // Show the winner if there is one.
        if (player != null) {
            SimpleUtil.sendPlayersMessage(getPlayers(),
                    ChatColor.YELLOW + player.getName() + ChatColor.GOLD + " has won the game!");
            player.sendTitle(ChatColor.BLUE + "You Win!", "", 0, 20, 10);
        } else {
            // Could happen but shouldn't
            SimpleUtil.sendPlayersMessage(getPlayers(),  ChatColor.GOLD + "Nobody won the game!");
        }

        super.endGame();
    }

    @Override
    protected void resetGame() {
        super.resetGame();

        SimpleRegion snowRegion = ((SpleefMapConfig) getMapConfig()).getSnowRegion();
        int y = snowRegion.getPos1().y();
        World world = getMapConfig().getSpawnLocation().getWorld();
        for (int x = snowRegion.getPos1().x(); x <= snowRegion.getPos2().x(); x++) {
            for (int z = snowRegion.getPos1().z(); z <= snowRegion.getPos2().z(); z++) {
                assert world != null;
                Block block = world.getBlockAt(x, y, z);
                if (block.getType().isAir()) {
                    block.setType(Material.SNOW_BLOCK);
                }
            }
        }
    }

/*----------------------------------------------------------------------------------------------------------------------
                                                    Game Methods
----------------------------------------------------------------------------------------------------------------------*/

    private void startGracePeriod() {
        graceTime = 5;
        gracePeriod = true;
        // Only create task if plugin is enabled (used to prevent errors when plugin shuts down)
        if (SimplePVPSpleef.getPlugin().isEnabled()) {
            gracePeriodTask = Bukkit.getScheduler().runTaskTimer(SimplePVPSpleef.getPlugin(), () -> {
                if (graceTime == 0) {
                    SimpleUtil.playPlayersGlobalSound(getPlayers(), Sound.BLOCK_NOTE_BLOCK_PLING, 2);
                    stopGracePeriod();
                }
                if (graceTime > 0) {
                    SimpleUtil.playPlayersGlobalSound(getPlayers(), Sound.BLOCK_NOTE_BLOCK_PLING, 1);
                    SimpleUtil.sendPlayersActionBar(getPlayers(),
                            ChatColor.GOLD + "Starting in  " + ChatColor.RED + graceTime);
                }
                graceTime--;
            }, 20, 20);
        }
    }

    // Stop the grace period.
    private void stopGracePeriod() {
        if (gracePeriodTask != null) {
            gracePeriodTask.cancel();
            gracePeriod = false;
        }
    }

/*----------------------------------------------------------------------------------------------------------------------
                                                    Game Events
----------------------------------------------------------------------------------------------------------------------*/

    @EventHandler
    public void onBlockBreak(BlockBreakEvent e) {
        if (hasPlayer(e.getPlayer())) {
            e.setDropItems(false);
            if (gracePeriod || !e.getBlock().getType().equals(Material.SNOW_BLOCK)) {
                e.setCancelled(true);
            }

            if (!gracePeriod) {
                if (e.getBlock().getType().equals(Material.SNOW_BLOCK)) {
                    e.getPlayer().getInventory().addItem(new ItemStack(Material.SNOWBALL));
                    e.getPlayer().playSound(e.getBlock().getLocation(), Sound.ENTITY_CHICKEN_EGG, 1, 1);
                }
            }
        }
    }

    @EventHandler
    public void onSnowballHit(ProjectileHitEvent e) {
        Block block = e.getHitBlock();
        Entity entity = e.getHitEntity();
        SimpleRegion snowRegion = ((SpleefMapConfig) getMapConfig()).getSnowRegion();

        if (!gracePeriod) {
            if (e.getEntity().getType().equals(EntityType.SNOWBALL)) {
                if (block != null && block.getType().equals(Material.SNOW_BLOCK)) {
                    if (snowRegion.inRegion(block.getX(), block.getY(), block.getZ())) {
                        block.setType(Material.AIR);
                        Objects.requireNonNull(block.getLocation().getWorld()).playSound(block.getLocation(), Sound.BLOCK_SNOW_BREAK, 1, 1);
                    }
                }

                if (entity instanceof Player player && player.getNoDamageTicks() <= 10) {
                    player.damage(0.00001);
                    player.setVelocity(new Vector(e.getEntity().getVelocity().getX()*0.6, 0.4, e.getEntity().getVelocity().getZ()*0.6));
                }
            }
        }
    }

}

package com.simplepvpspleef;

import com.simplepvp.MapConfig;
import com.simplepvp.util.SimpleRegion;
import org.bukkit.Location;
import org.bukkit.configuration.ConfigurationSection;

public class SpleefMapConfig extends MapConfig {

    private final Location pvpSpawnLocation;
    private final SimpleRegion snowRegion;

    public SpleefMapConfig(ConfigurationSection cs) {
        // Call the super.
        super(convertToTitleCase(cs.getName().replace('-', ' ')),
                cs.getInt("min-players"), cs.getInt("max-players"), cs.getLocation("spawn-location"));

        // Get the pvp spawn location and snow spawning region.
        pvpSpawnLocation = cs.getLocation("pvp-spawn-location");
        snowRegion = new SimpleRegion(
                cs.getInt("snow-region.first-corner.x"),
                cs.getInt("snow-region.first-corner.y"),
                cs.getInt("snow-region.first-corner.z"),
                cs.getInt("snow-region.second-corner.x"),
                cs.getInt("snow-region.second-corner.y"),
                cs.getInt("snow-region.second-corner.z")
        );
    }

    // Used to convert strings to title case. Used for map names.
    public static String convertToTitleCase(String text) {
        StringBuilder converted = new StringBuilder();

        boolean upperCaseNext = true;
        for (char c : text.toCharArray()) {
            if (Character.isSpaceChar(c)) {
                upperCaseNext  = true;
            } else if (upperCaseNext) {
                c = Character.toUpperCase(c);
                upperCaseNext  = false;
            } else {
                c = Character.toLowerCase(c);
            }
            converted.append(c);
        }

        return converted.toString();
    }

    // Getters.
    public Location getPvpSpawnLocation() {
        return pvpSpawnLocation;
    }

    public SimpleRegion getSnowRegion() {
        return snowRegion;
    }
}